We especially love to introduce our special kind of dental care to new patients and the youngest members of our community. We are honored when parents select our practice to get their children started on their way to a lifetime of healthy teeth. 

Address: 1876 Polk Street, Hollywood, FL 33020, USA

Phone: 954-923-7348

Website: https://www.smile-on-us.com/
